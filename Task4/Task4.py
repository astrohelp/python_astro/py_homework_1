class Rational(object):
    """класс рациональных дробей"""
    def reduce(self): #вспомогательная штука, жаль, нельзя вниз спрятать
        flag = False
        if self.numerator < 0:
            self.numerator = -self.numerator
            flag = True

        if self.numerator > self.denominator : maxval = self.numerator
        else : maxval = self.denominator

        i = 2
        while i < maxval:
            if (self.numerator % i == 0) & (self.denominator % i == 0):
                self.numerator //=  i
                self.denominator //= i
                i = i - 1
            i = i + 1
        if flag: self.numerator *= -1

    def __init__(self, numerator, denominator):
        if (denominator == 0): raise ZeroDivisionError
        if (denominator == 0): raise Exception("Ноль не представим в виде дроби") #наверное

        if numerator == denominator:
            self.numerator = self.denominator = 1
        elif ((numerator > 0) & (denominator > 0)) | ((numerator < 0) & (denominator < 0)):
            self.denominator = denominator
            self.numerator = numerator
            self.reduce()
        elif (numerator < 0) | (denominator < 0):
            self.denominator = abs(denominator)
            self.numerator = abs(numerator)
            self.reduce()
            self.numerator *= -1

    def __add__(self, other):
        result = Rational(self.numerator*other.denominator + other.numerator*self.denominator, self.denominator*other.denominator)
        result.reduce()
        return result

    def __sub__(self, other):
        result = Rational(self.numerator*other.denominator - other.numerator*self.denominator, self.denominator*other.denominator)
        result.reduce()
        return result
        
    def __mul__(self, other):
        result = Rational(self.numerator*other.numerator, self.denominator*other.denominator)
        result.reduce()
        return result

    def __truediv__(self, other):
        result = Rational(self.numerator*other.denominator, self.numerator*other.denominator)
        result.reduce()
        return result

a = Rational(1,1)
b = Rational(1,2)

print(a.numerator, a.denominator)
a*b