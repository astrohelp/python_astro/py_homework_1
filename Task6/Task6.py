#5 самых частых слов в файле

def dict_sorting(dictionary):
    first5 = []
    for w in sorted(dictionary, key=dictionary.get, reverse=True):
        if len(first5) == 5: break
        first5.append(w)
    print(first5)

def words_frequency(file_name):
    with open(file_name, 'r') as f:
        text = f.read()
    from collections import defaultdict
    d = defaultdict(int)
    for word in text.split():
      word = word.lower()
      d[word] += 1
    dict_sorting(d)

#words_frequency("TheGunslinger.txt") #надо парсить и удалять стоп-слова для начала, проблема со сплитом
words_frequency("test.txt")