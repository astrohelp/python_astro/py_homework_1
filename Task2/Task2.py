def show_file_info(file_name):
    """выводит число строк, слов и знаков в файле"""
    lines_count, words_count, chars_count = 0, 0, 0

    with open(file_name, 'r') as f:
        for line in f:
            lines_count += 1
            words = line.split(' ') # s.translate strip

            words_count += len(words)
            #chars_count += len(line) #если считать пробелы... и \r\n...
            for char in line:
                if char != ' ':
                    chars_count += 1 #если не считать пробелы

    chars_count -= lines_count #почему возврат каретки - это слово?!

    print("Строк:", lines_count)
    print("Слов:", words_count)
    print("Знаков:", chars_count)

show_file_info("rsync_call.txt")