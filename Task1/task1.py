#класс-прямоугольник

class Rectangle(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
    def area(self):
        return self.width * self.height
    def perimeter(self):
        return 2*(self.height+self.width)
    def draw(self, filled):
        if filled:
            inner = '#'
        else:
            inner = ' '
        for x in range(self.width):
            for y in range(self.height):
                if (x == 0) or (y == 0) or (x == self.width-1) or (y == self.height-1):
                    print('#', end='')
                else:
                    print(inner, end='')
            print()

a = Rectangle(1,1)
a.draw(False)
print(a.area())