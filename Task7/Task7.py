# вычисляет число пи
import Task4

m = int(input("Max iteration number = "))

i = 0
pi2 = Task4.Rational(1,1)
numerator = 0
denominator = 1

print("n =   ", "PI = ")
for n in range(1,m):
    if i%2 == 0:
        numerator += 2
    else:
        denominator += 2
    franck = Task4.Rational(numerator, denominator)
    
    pi2 = pi2 * franck
    pi = 2 * pi2.numerator / pi2.denominator
    print(n, "    ", pi)
    i += 1