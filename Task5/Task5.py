#Выводит n-ое простое число

def Find_n_prime(n):
    from math import sqrt

    prime_numbers = []

    number = 2
    while (len(prime_numbers) != n):
        for prime_number in prime_numbers:
            if prime_number > int((sqrt(number)) + 1):
                prime_numbers.append(number)
                break
            if (number % prime_number == 0):
                break
        else:
            prime_numbers.append(number)
        number += 1

    return(prime_numbers[-1])
